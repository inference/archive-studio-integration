/**
 * @file provides simple javascript Studio API consumer
 * @author Roger Venning
 *
 *  Copyright (c) 2017 Inference Solutions
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * utility function to obtain a Inference Studio API token
 * @description The Inference Studio API utilises a token acquisition process for authenticating subsequent requests. 
 * Relies on script properties tokenUrl, username & password to be set as per Studio API documentation
 * @todo Cache the token as it lasts for 15 minutes, and handle expiration and re-acquisition
 * @returns the token as a {string}
 */
function getToken() {
  //---Digest authentication in Google Apps Script relies on http://excelramblings.blogspot.com.au/2013/05/digest-authentication-and-google-apps.html
  var props = PropertiesService.getScriptProperties()
  var authUrl = props.getProperty('tokenUrl')
  var creds = {
    'username': props.getProperty('username'),
    'password': props.getProperty('password')
  }
  
  Logger.log(JSON.stringify(creds))
  // do digest auth dance
  var d = new cDigestAuth (authUrl,creds).login();           
  // do something
  if (d.isLoggedIn()) {
    Logger.log('getToken: sucessfully digest authenticated')
    var options = {
      'method': 'post',
      'headers': {
        'Authorization': d.digest()
      },
      'payload': {
        'apikey': props.getProperty('apikey'),
        'format': 'json'
      }
    }
    
    var response = UrlFetchApp.fetch(authUrl, options) 
    // muteHttpException is no longer asserted so any failure will render(?)
    
    var token = JSON.parse(response.getContentText()).result.token
    Logger.log('getToken: successfully obtained token')
    return token
    
  }
  else {
    Logger.log('getToken: failed to digest authenticate. Check authUrl, username, password in script properties.')
    return undefined
  }
}

/**
 * getSingleValue from a data store
 * @description Obtains a single value from specified target 
 * @todo make generic for value, row and lists
 * @param {numeric} datastore_id The Studio data store identifier as shown in data store list. This data store must have been enabled for web configurator access in Studio
 * @param {numeric} data_id The tuple id from the selected data store.
 * @param {string} column_name The data store column name holding the targeted data
 * @param {string} salt An unused parameter to allow forced refresh within spreadsheet
 * @returns the value as string regardless of data store storage format?
 * @customfunction
 */
function getSingleValue(datastore_id, data_id, column_name) {
  session = getToken()
    
  var url = 'https://ausstudio.inferencecommunications.com/studio_instance/studio-api/v1/datastore/get-value/'  
  var options = {
      'method': 'post',
      'payload': {
        'token': session,
        'datastore_id': datastore_id,
        'data_id': data_id,
        'column_name': column_name,
        'format': 'json'
      }
    }
  var response = UrlFetchApp.fetch(url, options)
  Logger.log("getSingleValue: " + response.getContentText())
  return JSON.parse(response.getContentText()).result
}


/**
 * setSingleValue within a data store
 * @description Sets a single value at the specified target 
 * @todo make generic for value, row and lists.
 * @todo error handling when setting to unknown destinations or incorrect data type.
 * @param {numeric} datastore_id The Studio data store identifier as shown in data store list. This data store must have been enabled for web configurator access in Studio
 * @param {numeric} data_id The tuple id from the selected data store.
 * @param {string} column_name The data store column name holding the targeted data
 * @param {string} val The value to set. 
 * @returns the value as string regardless of data store storage format?
 * @customfunction
 */
function setSingleValue(datastore_id, data_id, column_name, val) {
  session = getToken()
    
  var url = 'https://ausstudio.inferencecommunications.com/studio_instance/studio-api/v1/datastore/update-value/'  
  var options = {
      'method': 'post',
      'payload': {
        'token': session,
        'datastore_id': datastore_id,
        'data_id': data_id,
        'column_name': column_name,
        'column_value': val,
        'format': 'json'
      }
    }
  var response = UrlFetchApp.fetch(url, options)
  return "Value set!"
}

