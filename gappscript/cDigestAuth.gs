/**
 * @file implements Digest Authentication in javascript in a Studio compatible manner (must initiate auth with POST method if target is a URL requiring POST method)
 * @author Bruce McPherson
 * @author Roger Venning
 *
 *  Adapted from http://ramblings.mcpher.com/ released under https://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * 
 *  Copyright (c) 2014,2017 Bruce McPherson, Inference Solutions
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Digest authenticate to Studio as per RFC 2617
 * @description Digest authenticate to the target URL using POST method as required by SZtudio
 * @returns a digest object from which d.digest() can be used in subsequent Authorization headers
 */
function cDigestAuth(url, credentials) {
  this.credentials = credentials;
  this.url = url;
  this.login = function () {
    this.cnonce = new Date().getTime().toString(16);
    return this.initWorkflow().finishWorkflow();
  };
  this.isLoggedIn = function () {
    return (this.danceStep2 && this.danceStep2.getResponseCode() != 401);
  };
  this.initWorkflow = function () {
    this.danceStep1 = UrlFetchApp.fetch(this.url, {"method":"POST", "muteHttpExceptions":true});
    Logger.log("cDigestAuth: step 1 done - " + this.danceStep1.getResponseCode())
    return this;
  };
  this.finishWorkflow = function () {
    var options = {"method":"POST", "muteHttpExceptions":true, "headers":{"Authorization":this.digest()}};
    Logger.log("cDigestAuth: log in with " + JSON.stringify(this.digest()))
    this.danceStep2 = UrlFetchApp.fetch(this.url, options);
    Logger.log("cDigestAuth: step 2 done - " + this.danceStep2.getResponseCode())
    Logger.log("cDigestAuth: step 2 done - " + JSON.stringify(this.danceStep2.getAllHeaders()))
    Logger.log("cDigestAuth: step 2 done - " + this.danceStep2.getContentText())
    return this;
  };
  this.digest = function () {
    var nc = "1";
    var o = this.authSplit();
    Logger.log("Auth split: " + JSON.stringify(o))
    if (o.algorithm && o.algorithm != "MD5") {
      throw ("cDigestAuth: unable to deal with requested algorithm " + o.algorithm);
    }
    o.algorithm = "MD5";
    var HA1 = bytesToHex(md5(this.credentials.username + ":" + o.realm + ":" + this.credentials.password));
    var HA2 = bytesToHex(md5("POST" + ":" + this.url));
    
    // this implemention only supports qop=auth
    o.qop = "auth"
    var response = bytesToHex(md5(HA1 + ":" + o.nonce + ":" + nc + ":" + this.cnonce + ":" + o.qop + ":" + HA2));
    
    
    var digest = "Digest username=\"" + this.credentials.username + "\"" + ",realm=\"" + o.realm + "\"" + ",nonce=\"" + o.nonce + "\"" + ",uri=\"" + this.url + "\"" + ",qop=" + o.qop + ",nc=" + nc + ",algorithm=" + o.algorithm + ",cnonce=\"" + this.cnonce + "\"" + ",response=\"" + response + "\"";
    if (o.opaque) {
      digest += ",opaque=\"" + o.opaque + "\"";
    }
    return digest;
  };
  this.authSplit = function () {
    return authSplit(this.danceStep1.getHeaders()["WWW-Authenticate"]);
  };
  this.header200 = function () {
    return this.danceStep2.getHeaders();
  };
  function md5(s) {
    return Utilities.computeDigest(Utilities.DigestAlgorithm.MD5, s);
  }
  function bytesToHex(b) {
    var s = "";
    for (var i = 0; i < b.length; i++) {
      var by = b[i] < 0 ? b[i] + 256 : b[i];
      var t = maskString(by.toString(16), "00");
      s += t;
    }
    return s;
  }
  function maskString(sIn, f) {
    var s = sIn.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
    if (s.length < f.length) {
      s = f.slice(0, f.length - s.length) + s;
    }
    return s;
  }
  function authSplit(aHeader, optSplitChar) {
    var a = aHeader.replace(/^"|"$/g, "").replace(/^Digest /, "") //.split(optSplitChar || ",");
    var params = a.match(/(\w+)[:=] ?"?([\w\s,-.]+)"?/g)
    var o = {}
    for (var i = 0; i < params.length; i++) {
      attrval = params[i].match(/(\w+)[:=] ?"?([\w\s,-.]+)"?/)
      if (attrval.length != 3)
        throw ("error in WWW-Authenticate response " + a.join());
      
      o[attrval[1]] = attrval[2].replace(/^"|"$/g, "");
    }
    Logger.log("cDigestAuth: digest properties: " + JSON.stringify(o))
    
    return o;
  }
}
