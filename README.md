## Inference Studio API Integration Tools

This repository provides tools for consuming [Inference Solutions'](https://www.inferencesolutions.com) [Studio APIs](https://inferencesolutions.com/studio) as documented in the [Inference Solutions Resource Center](docs.inferencesolutions.com).

The materials here are provided under [GNU Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).

---

## Google Apps Script - gappscript

This code shows an example of how to obtain a token and access the data-store API from Javascript, specifically Google Apps Script.
